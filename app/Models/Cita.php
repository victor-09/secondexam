<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = ["nombrecliente", "contactocliente", "nombremascota", "tipomascota", "edadmascota", "propositocita", "pesomascota"];
}
