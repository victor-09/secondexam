<?php

use App\Models\Cita;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "username" => "admin",
            "password" => Hash::make('admin')
        ]);

        Cita::create([
            "nombrecliente" => "nombrecliente",
            "contactocliente" => "contactocliente",
            "nombremascota" => "nombremascota",
            "tipomascota" => "tipomascota",
            "edadmascota" => "edadmascota",
            "propositocita" => "propositocita",
            "pesomascota" => "pesomascota",
        ]);

    }
}

